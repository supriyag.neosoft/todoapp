<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\TodoList;
use View;
use Cookie;

class IndexController extends Controller
{
    public function index()
    {
        $todo_list = Cookie::get('cookie_tl_arr');
        // dd($todo_list);
        return view('tasks')->with('todo_list', $todo_list);
    }

    public function store(Request $request)
    {
        if(Cookie::get('cookie_tl_arr') !== null) {
            $cookie_tl_arr = Cookie::get('cookie_tl_arr');
        }
        else {
            $cookie_tl_arr = array();
        }
        
        Cookie::queue('todo_l', 'sdfsf', 2628000);

        $todo = new TodoList();
        $todo->name = $request->name;
        //$todo->user_agent = $request->header('User-Agent');
        $todo->save();

        $task = array("task_id" => $todo->id, "name" => $request->name, "is_complete" => "0");
        
        array_push($cookie_tl_arr, $task);
        Cookie::queue('cookie_tl_arr', $cookie_tl_arr, 2628000);
        
        return redirect()->route('index');
    }

    public function destroy(Request $request)
    {
        $todo_list = Cookie::get('cookie_tl_arr');

        $todo_list_new = $this->removeElementWithValue($todo_list, 'task_id', $request->taskId);
        // dd($todo_list_new);
        Cookie::forget('cookie_tl_arr');
        Cookie::queue('cookie_tl_arr', $todo_list_new, 2628000);

        $todo = TodoList::findorFail($request->taskId);
        
        $todo->delete();
        return 1;

    }

    public function update_status(Request $request)
    {
        $todo_list_arr = Cookie::get('cookie_tl_arr');

        foreach($todo_list_arr as $subKey => $subArray) {
            // echo "subkey = ".$subKey;
            // print_r($subArray);
            if($subArray['task_id'] == $request->taskId){
                // echo "matched = ".$subArray['task_id'];
                $todo_list_arr[$subKey]['is_complete'] = $request->status;
            }
        }
        
        Cookie::forget('cookie_tl_arr');
        Cookie::queue('cookie_tl_arr', $todo_list_arr, 2628000);
        
        $todo = TodoList::find($request->taskId);
        $todo->is_complete = $request->status;
        if($todo->save()) {
            return 1;
        } else {
            return 0;
        }
    }
 
    public function removeElementWithValue($array, $key, $value){
        foreach($array as $subKey => $subArray){
            // echo "subkey = ".$subKey;
            // print_r($subArray);
            if($subArray[$key] == $value){
                // echo "matchecd = ".$subArray[$key];
                unset($array[$subKey]);
            }
        }
        return $array;
   }
}
