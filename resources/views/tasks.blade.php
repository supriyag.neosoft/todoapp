@extends('layouts.app')

@section('css')

@endsection
    
@section('content')

    <!-- Bootstrap Boilerplate... -->

    <div class="panel-body">
        <!-- Display Validation Errors -->
        @include('common.errors')
        <h2 style="text-align: left">To Do List</h2>
        <!-- New Task Form -->
        <form id="todo-form" method="POST" action="{{ route('task.store') }}" class="form-horizontal" autocomplete="off">
            {{ csrf_field() }}

            <!-- Task Name -->
            <div class="form-group">
                {{-- <label for="name" class="col-sm-2 control-label">Task</label> --}}
                {{-- <div class="col-sm-2">
                </div> --}}
                <div class="col-sm-6">
                    <input type="text" name="name" id="name" class="form-control">
                </div>
                <div class="col-sm-4">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-plus"></i> Add Task
                    </button>
                </div>
            </div>

            <!-- Add Task Button -->
            {{-- <div class="form-group">
                <div class="col-sm-offset-3 col-sm-6">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-plus"></i> Add Task
                    </button>
                </div>
            </div> --}}
        </form>
    </div>

    @if($todo_list)
    <!-- TODO: Current Tasks -->
    <div class="panel panel-default">
        
        <div class="panel-heading">
            Current Tasks
        </div>

        <div class="panel-body">
            <table class="table table-bordered table-hover table-sm">

                <!-- Table Headings -->
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">List</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>

                <!-- Table Body -->
                <tbody id="task-list-tbody">
                    @foreach($todo_list as $tl)
                    <tr>
                        <td>
                            <input type="checkbox" {{ $tl['is_complete'] != 1 ? "" : "checked" }} value="{{ $tl['task_id'] }}" class="update-status-td">
                        </td>
                        <td class="table-text">
                            <div style="text-decoration: {{ $tl['is_complete'] != 1 ? 'none' : 'line-through' }}">{{ $tl['name'] }}</div>
                        </td>
                        <td>
                            <a href="#" class="delete-task-td btn btn-xs btn-danger" data-id="{{ $tl['task_id'] }}"><i class="glyphicon glyphicon-trash"></i> 
                            Delete
                            </a>
                        </td>
                    </tr>
                    @endforeach
                    
                </tbody>
            </table>
        </div>
    </div>
    @endif

@endsection

@section('script')
<script>
$("#todo-form").validate({
// debug: true,
    rules: {
        name: {
            required: true,
            minlength: 2
        }
    },
    message: {
        name: {
            required: "Please enter task",
            minlength: "Task must be at least 2 characters"
        }
    }
});

if($("#task-list-tbody tr").length != 0) {
    $(".panel-default").show();
}

/*** Delete task --START***/
$(".delete-task-td").on('click', function() {
    console.log($(this).parent().parent());
    console.log($(this).attr('data-id'));

    var taskId = $(this).attr('data-id');
    var $this = $(this);
    
    $.ajax({
        url: "{{ route('task.destroy') }}",
        type: "POST",
        data: {
            taskId: taskId,
            _token: "{{ csrf_token() }}"
        },
        success: function(result) {
            // window.location.href = "{{ route('index') }}";
            $this.parent().parent().remove();
            console.log(result);
            console.log($("#task-list-tbody tr").length);

            if($("#task-list-tbody tr").length == 0) {
                $(".panel-default").hide();
            } 
        }
    });
});
/*** Delete task --END***/

/*** update complete task status --START***/
$('.update-status-td').click(function(){
    var status;
    if($(this).prop("checked") == true){
        status = 1;
    }
    else if($(this).prop("checked") == false){
        status = 0;
    }
    
    var taskId = $(this).val();
    var $this = $(this);
    
    $.ajax({
        url: "{{ route('task.update-status') }}",
        type: "POST",
        data: {
            taskId: taskId,
            status: status,
            _token: "{{ csrf_token() }}"
        },
        success: function(result) {
            console.log(result);

            if(result == 1) 
            {
                if(status == 1) {
                    $this.parent().next().find('div').css('text-decoration','line-through');
                } else {
                    $this.parent().next().find('div').css('text-decoration','none');
                }
            } 
            else 
            {
                console.log('Something went wrong');
            }
        }
    });
}); 
/*** update complete task status --END***/ 
</script>
@endsection
