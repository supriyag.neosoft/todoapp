<!DOCTYPE html>
<html lang="en">
    <head>
        <title>To Do App</title>

        <!-- CSS And JavaScript -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
        <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
        
        @yield('css')
        <style>
        .error {
            color: red;
        }
        </style>
    </head>

    <body>
        <div class="container">
            
                <!-- Navbar Contents -->
                
            
            @yield('content')
        </div>
        
        <script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
        
        <script>
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        </script>
        @yield('script')
    </body>
</html>