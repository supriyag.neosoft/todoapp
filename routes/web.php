<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Route::get('/', function () {
    return view('welcome');
}); */

Route::get('/', 'IndexController@index')->name('index');
Route::post('/task/store', 'IndexController@store')->name('task.store');
Route::post('/task/destroy', 'IndexController@destroy')->name('task.destroy');
Route::post('/task/update-status', 'IndexController@update_status')->name('task.update-status');


Route::get('/cookie/set','IndexController@setCookie');
Route::get('/cookie/get','IndexController@getCookie');